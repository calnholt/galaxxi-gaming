import React from "react";

export const MEET_THE_TEAM = "/meet-the-team";
export const SPOILERS = "/spoilers";
export const SPONSORS = "/sponsors";
export const LOGIN = "/login";