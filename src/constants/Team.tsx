export const TEAM_MEMBERS = ['chris', 'calvin', 'elijah', 'noah', 'joe', 'patrick', 'bernard'].sort((a, b) => a.localeCompare(b));