import React, { useEffect, useState } from "react";
import styled from "styled-components";
import logo from "../../images/Galaxxi_Gaming_Logo_L.png";
import { Link, useLocation } from "react-router-dom";
import { LOGIN, MEET_THE_TEAM, SPOILERS, SPONSORS } from "src/constants/RouteConstants";
import { useUser } from "../user/UserContext";

const HeaderContainer = styled.div`
  height: 4rem;
  background-color: #6a0689;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 100;
  display: flex;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid white;
`;

const LogoContainer = styled(Link)`

`;

const NavLinkContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const NavLink = styled(Link)`
  cursor: default;
  font-size: 1.5rem;
  color: white;
  cursor: pointer;
  padding: 0rem 1rem 0rem 1rem;
  text-decoration: unset;
  font-family: roboto;
  :hover {
    text-decoration: underline;
  }
  &.active {
    text-decoration: underline;
  }
`;

const GalaxxiLogo = styled.img`
  min-width: 100px;
  width: 240px;
  position: absolute;
  left: 0;
  cursor: pointer;
  top: 0;
`;

export const Header: React.FC = () => {
  const location = useLocation();
  const { userState } = useUser();
  const [actviveTab, setActiveTab] = useState<string>(location.pathname);

  useEffect(() => {
    setActiveTab(location.pathname);
  }, [location.pathname])

  return (
    <HeaderContainer>
      <LogoContainer to={""}>
        <GalaxxiLogo src={logo}/>
      </LogoContainer>
      <NavLinkContainer>
        <NavLink to={MEET_THE_TEAM} className={actviveTab === MEET_THE_TEAM ? "active" : ""}>Meet the Team</NavLink>
        <NavLink to={SPOILERS} className={actviveTab === SPOILERS ? "active" : ""}>Spoilers</NavLink>
        <NavLink to={SPONSORS} className={actviveTab === SPONSORS ? "active" : ""}>Sponsors</NavLink>
        {!userState.isTeam() &&
          <NavLink to={LOGIN} className={actviveTab === LOGIN ? "active" : ""}>Login</NavLink>
        }
      </NavLinkContainer>
    </HeaderContainer>
  )
}