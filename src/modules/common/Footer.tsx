import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { PURPLE } from "src/constants/Styles";
import styled from "styled-components";
import { faYoutube, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { TWITTER, YOUTUBE } from "src/constants/Links";

const Container = styled.div`
  padding: 1rem;
  display: flex;
  background-color: ${PURPLE};
  width: 100%;
  justify-content: space-evenly;
}
`;

const Item = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
}
`;

const Title = styled.div`
  color: white;
  font-size: 2rem;
  margin-bottom: 1rem;
  font-family: roboto;
`;

const Email = styled.div`
  color: white;
`;

const IconsContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  width: 100%;
`;

const Icon = styled(FontAwesomeIcon)`
  cursor: pointer;
`;

export const Footer: React.FC = () => {

  const openLink = (url: string) => {
    window.open(url);
  }

  return (
    <Container>
      <Item>
        <Title>Contact</Title>
        <Email>galaxxigaming@gmail.com</Email>
      </Item>
      <Item>
        <Title>Follow</Title>
        <IconsContainer>
          <Icon 
            color={"white"}
            size="2x" 
            icon={faYoutube}
            onClick={() => openLink(YOUTUBE)}
          />
          <Icon 
            color={"white"} 
            size="2x" 
            icon={faTwitter}
            onClick={() => openLink(TWITTER)}
          />
        </IconsContainer>
      </Item>
    </Container>
  );

}