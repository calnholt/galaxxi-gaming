import React from "react";
import { PURPLE } from "src/constants/Styles";
import styled from "styled-components";
import parse from "html-react-parser";

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 3rem;
  `;
  
  export const Container = styled.div`
  padding: 1rem;
  display: flex;
  background-color: ${PURPLE};
  border-radius: 5px;
  width: 65rem;
  align-items: center;
  box-shadow: 10px 10px 8px #e1c0ff;
`;

export const Name = styled.div`
  font-size: 30px;
  color: white;
  margin-left: 1rem;
  font-weight: 700;
  text-decoration: underline;
`;

const Image = styled.img`
  border-radius: 5px;
  max-height: 25rem;
  max-width: 25rem;
`;

const Title = styled.div`
  font-size: 30px;
  color: white;
  margin-left: 1rem;
  font-weight: 700;
  text-decoration: underline;
`;

const Text = styled.div`
  padding: 1rem;
  color: white;
  font-size: 20px;
  width: 100%;
  white-space: break-spaces;
`;

type CardProps = {
  src: string;
  title?: string;
  text: string;
  isRight: boolean;
}

export const Card: React.FC<CardProps> = ({ src, title, text, isRight }) => {
  return (
    <Wrapper>
      <Container>
        {isRight ? (
          <>
            {title ? (
              <>
                <div>
                  <Title>{title}</Title>
                  <Text>{parse(text)}</Text>
                </div>
                <Image src={src} />
              </>
            ) : (
              <>
                <Text>{text}</Text>
                <Image src={src} />
              </>
            )}
          </>
        ) : (
          <>
            {title ? (
              <>
                <Image src={src} />
                <div>
                  <Title>{title}</Title>
                  <Text>{parse(text)}</Text>
                </div>
              </>
            ) : (
              <>
                <Image src={src} />
                <Text>{text}</Text>
              </>
            )}
          </>
        )}
      </Container>
    </Wrapper>
  )
}