import React from "react";
import styled from "styled-components";

type Props = {
  src: string;
  style?: React.CSSProperties;
};

const Container = styled.div`
  position: relative;
  width: 100%;
  height: 0;
  padding-bottom: 56.25%;
`;

const DefaultStyle: React.CSSProperties = {
  border: "unset",
  maxWidth: "1125px",
  maxHeight: "632px",

};

export const EmbeddedVideo: React.FC<Props> = ({ src, style }) => {
  return (
    <Container>
      <iframe 
        src={src}
        style={{...DefaultStyle, ...style}}
        title="YouTube video player"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
        allowFullScreen>
      </iframe>
    </Container>
  )
};