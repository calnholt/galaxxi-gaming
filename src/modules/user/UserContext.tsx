import { useEasybase } from "easybase-react";
import React, { createContext, useContext, useEffect, useReducer } from "react";

export type User = {
  role: string;
  name: string;
}

type UserState = {
  user: any;
  isTeam: () => boolean;
};

type UserContext = {
  userState: UserState;
  dispatchUser: React.Dispatch<any>;
};

const initialState: UserState = {
  user: null,
  isTeam: null,
};

const UserContext = createContext<UserContext>({
  userState: initialState,
  dispatchUser: null,
});

const UserAction = {
  LOGIN: "LOGIN",
};

const reducer = (state: UserState, action: any) => {
  switch(action.type) {
    case UserAction.LOGIN:
      return {
        ...state,
        user: action.payload,
      };
    default:
      return {...state};
  }
};

const UserContextProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { getUserAttributes } = useEasybase();

  useEffect(() => {
    getUserAttributes().then(res => {
      if (res && res.role === "TEAM") {
        dispatch({
          type: UserAction.LOGIN,
          payload: res,
        });
      }
    });
  }, []);

  const isTeam = (): boolean => Boolean(state.user && state.user.role === "TEAM");

  return (
    <UserContext.Provider
      value={{
        userState: {
          ...state,
          isTeam: isTeam,
        },
        dispatchUser: dispatch,
      }}
    >
      { children }
    </UserContext.Provider>
  );
};

const useUser = (): UserContext => {
  const context = useContext<UserContext>(UserContext);

  if (context === undefined) {
    throw new Error("useUser must be used within a UserContextProvider");
  }

  return context;
};

export { UserContextProvider, useUser, UserAction };
