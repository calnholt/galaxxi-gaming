
import { Auth, useEasybase } from "easybase-react";
import React, { useEffect, useRef } from "react";
import { UserAction, useUser } from "../user/UserContext";


const Login: React.FC = () => {
  const { getUserAttributes, signOut } = useEasybase();
  const { dispatchUser } = useUser();

  useEffect(() => {
    getUserAttributes().then(res => console.log(res));
  },  []);

  const onSuccess = () => {
    getUserAttributes().then(res => {
      if (res && res.role === "TEAM") {
        dispatchUser({
          type: UserAction.LOGIN,
          payload: res,
        });
      }
    });
    return (<p>You're in!</p>);
  }

  return (
    <Auth>
      {onSuccess()}
    </Auth>
  );

};

export default Login;