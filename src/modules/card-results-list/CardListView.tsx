import React from "react"
import { CardResultsList } from "./CardResultsList"
import { CardResultsContextProvider } from "./CardResultsContext"
import { CardFilters } from "./CardFilters"


export const CardListView: React.FC = ({}) => {
  return (
    <CardResultsContextProvider>
      <CardFilters />
      <CardResultsList />
    </CardResultsContextProvider>
  )
}