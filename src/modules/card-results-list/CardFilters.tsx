
import React from 'react';
import styled from 'styled-components';
import Select, {MultiValue} from 'react-select';
import {CARD_TYPES, CLASS_TYPES, RARITY_TYPES, SET_TYPES, TALENT_TYPES} from '../../types/card-types';
import {useEffect} from 'react';
import {useEasybase} from 'easybase-react';
import {CardResultAction, useCardResults} from './CardResultsContext';

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  align-items: center;
  justify-content: center;
`;
const Label = styled.div`

`;

const FilterContainer = styled.div`
  margin-top: 5px;
  width: 25rem;
  margin-right: 2rem;
`;

const SearchInput = styled.input`
  padding: 0px 9px;
  width: 100%;
  border: 1px solid #cccccc;
  border-radius: 4px;
  height: 34px;
  font-family: inherit;
  font-size: 16px;
  color: #808080;

  :focus-visible {
    outline: -webkit-focus-ring-color auto 1px;
    outline-color: #2684ff;
    outline-style: auto;
    outline-width: 1px;
  }
`;

interface Filter {
  label: string;
  options: (number | string)[];
  property: string;
  isSorted?: boolean;
};

export type SearchFields = {
  cardType?: string[];
  rarity?: string[];
  talent?: string[];
  class?: string[];
  setName?: string[];
}

export const CardFilters: React.FC = ({ }) => {
  const {db} = useEasybase();
  const {cardResultsState, dispatchCardResults} = useCardResults();

  // TODO: update order of filters
  const filters: readonly Filter[] = [
    {
      label: 'Card Type',
      options: CARD_TYPES,
      property: 'cardType',
      isSorted: true,
    },
    {
      label: 'Rarity',
      options: RARITY_TYPES,
      property: 'rarity',
      isSorted: true,
    },
    {
      label: 'Talent',
      options: TALENT_TYPES,
      property: 'talent',
      isSorted: true,
    },
    {
      label: 'Class',
      options: CLASS_TYPES,
      property: 'class',
      isSorted: true,
    },
    {
      label: 'Set',
      options: SET_TYPES,
      property: 'setName',
      isSorted: true,
    },
    {
      label: 'Pitch',
      options: [0, 1, 2, 3],
      property: 'pitch',
    },
    {
      label: 'Attack',
      options: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      property: 'attack',
    },
    {
      label: 'Defense',
      options: ['None', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      property: 'defense',
    },
  ];

  const search = () => {
    const searchAll = cardResultsState.searchFields == null || Object.keys(cardResultsState.searchFields).length === 0;
    if (searchAll) {
      db('CARDS').return().orderBy({by: 'name', sort: 'asc'}).all().then((data) => {
        dispatchCardResults({
          type: CardResultAction.SEARCH,
          payload: data,
        });
      });
    } else {
      db('CARDS').return().where(cardResultsState.searchFields).orderBy({by: 'name', sort: 'asc'}).all().then((data) => {
        dispatchCardResults({
          type: CardResultAction.SEARCH,
          payload: data,
        });
      });
    }
  };

  const updateFilters = async (property: string, values: MultiValue<{ value: any, label: any }>) => {
    let payload = null;
    if (values.length) {
      payload = values.map((v) => v.value);
    }
    dispatchCardResults({
      type: CardResultAction.FILTERS_UPDATED,
      payload: {[property]: payload},
    });
  };

  const updateSearchFilter = (value: string) => {
    let filteredCards = null;
    if (!value || value === '') {
      filteredCards = cardResultsState.searchResults;
    } else {
      filteredCards = cardResultsState.searchResults.filter((c) => String(c.name).toLowerCase().includes(value.toLowerCase()) || String(c.text).toLowerCase().includes(value.toLowerCase()));
    }
    dispatchCardResults({
      type: CardResultAction.FILTER_RESULTS,
      payload: filteredCards,
    });
  };

  const getOptions = (options: (string | number)[]) => {
    return options.map((option) => {
      return {label: option, value: option};
    });
  };

  const getOptionsSorted = (options: (string | number)[]) => {
    return options.map((option) => {
      return {label: option, value: option};
    }).sort((a, b) => String(a.value).localeCompare(String(b.value)));
  };

  useEffect(() => {
    search();
  }, [cardResultsState.searchFields]);

  return (
    <Container>
      {filters.map((filter) => {
        return (
          <FilterContainer key={filter.property}>
            <Label>{filter.label}</Label>
            <Select
              isMulti
              defaultValue={cardResultsState.searchFields?.[filter.property] ? {label: cardResultsState.searchFields[filter.property], value: cardResultsState.searchFields[filter.property]} : null}
              name={filter.label}
              options={filter.isSorted ? getOptionsSorted(filter.options) : getOptions(filter.options)}
              onChange={(newValue) => updateFilters(filter.property, newValue)}
            />
          </FilterContainer>
        );
      })}
      <FilterContainer>
        <Label>Search</Label>
        <div>
          <SearchInput
            type="text"
            onChange={(newValue) => updateSearchFilter(newValue.target.value)}
            placeholder="Search..."
          />
        </div>
      </FilterContainer>
    </Container>
  );
};
