import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { PURPLE } from "src/constants/Styles";
import { TEAM_MEMBERS } from "src/constants/Team";
import styled from "styled-components";
import { useUser } from "../user/UserContext";
import { getStars } from "./CardHelper";
import { faEdit, faTimesCircle, faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { TextField, Rating } from "@mui/material";
import { useEasybase } from "easybase-react";
import { CardResultAction, useCardResults } from "./CardResultsContext";

type Props = {
  card: Record<string, any>;
  isOpen: boolean;
  close: () => void;
}
const Container = styled.div`
  display: flex;
`;
const Users = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 1rem;
`;
const UserContainer = styled.div`
  margin-bottom: 1rem;
  padding: 0.5rem;
  border: 2px solid #6a068945;
  border-radius: 12px;
`;
const RatingContainer = styled.div<{ hasComments: boolean }>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: ${props => props.hasComments ? "1px solid #d7bcdf" : ""};
`;
const Name = styled.div`
  font-size: 22px;
  display: flex;
  align-items: center;
`;
const StarContainer = styled.div`

`;
const Comments = styled.div`
  margin-top: 0.5rem;
`;
const Image = styled.img`
  max-height: 25rem;
`;
const Icon = styled(FontAwesomeIcon)`
  height: 15px;
  cursor: pointer;
`;

export const CardRatingModal: React.FC<Props> = ({ card, isOpen, close }) => {
  const { dispatchCardResults } = useCardResults();
  const { userState } = useUser();
  const { db } = useEasybase();
  const isTeam = userState.isTeam();
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const ratingProp = `${userState?.user?.name}rating`;
  const commmentsProp = `${userState?.user?.name}comments`;
  const [userComments, setUserComments] = useState<string>(card[commmentsProp]);
  const [userRating, setUserRating] = useState<number>(card[ratingProp] ? card[ratingProp] : 0);

  const getUserName = (n: string): string => `${n.substring(0, 1).toLocaleUpperCase()}${n.substring(1, n.length)}`;

  const save = () => {
    setIsEditing(false);
    db("CARDS").where({ _key: card._key }).set({
      [`${userState?.user?.name}Comments`]: userComments,
      [ratingProp]: userRating
    }
    ).all().then(res => {
      //TODO: this is breaking the card context?!?!
      // dispatchCardResults({
      //   type: CardResultAction.UPDATE,
      //   payload: {...card, [commmentsProp]: userComments, [ratingProp]: userRating},
      // })
    })
  };

  const cancel = () => {
    setUserComments(card[`${userState.user.name}comments`]);
    setUserRating(card[`${userState.user.name}rating`]);
    setIsEditing(false);
  }

  const getNameComponent = (x: string) => {
    const name = getUserName(x);
    return (
      <Name>
        {isTeam && userState?.user?.name === x && !isEditing &&
          <Icon color={PURPLE} onClick={() => setIsEditing(true)} icon={faEdit} />
        }
        {isTeam && userState?.user?.name === x && isEditing &&
          <>
            <Icon color={PURPLE} onClick={() => cancel()} icon={faTimesCircle} />
            <Icon color={PURPLE} onClick={() => save()} icon={faCheckCircle} />
          </>
        }
        <>
          {name}
        </>
      </Name>
    )
  };



  const getCommentsComponent = (x: string) => {
    const comments = card[`${x}comments`];
    return (
      <>
        {isEditing && userState?.user?.name === x &&
          <TextField
            fullWidth
            multiline
            placeholder="Enter comments..."
            value={userComments}
            onChange={(event) => setUserComments(event.target.value)}
          />
        }
        {!isEditing && isTeam && userState?.user?.name === x &&
          <Comments>{userComments}</Comments>
        }
        {userState?.user?.name !== x &&
          <Comments>{comments}</Comments>
        }
      </>
    )
  }


  const getRatings = () => {
    return TEAM_MEMBERS.map(x => {
      const rating = card[`${x}rating`];
      const hasRating = rating !== null;
      const comments = card[`${x}comments`];
      const hasComments = comments !== null && comments !== "";
      if (!hasRating && !hasComments && userState?.user?.name !== x) {
        return <></>
      }
      return (
        <UserContainer>
          <RatingContainer hasComments={hasComments || isEditing}>
            {getNameComponent(x)}
            {(!isEditing ||!isTeam) &&
              <StarContainer>
                {getStars(card, userState?.user?.name !== x ? rating : userRating)}
              </StarContainer>
            }
            {isEditing && userState?.user?.name === x &&
              <Rating
                name="simple-controlled"
                value={userRating}
                onChange={(event, newValue) => {
                  setUserRating(newValue);
                }}
              />
            }
          </RatingContainer>
          {getCommentsComponent(x)}
        </UserContainer>
      )
    })
  }

  return (
    <Modal show={isOpen} onHide={() => {setIsEditing(false); close()}} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>{card.name} - Team Ratings</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Image src={card.image} />
          <Users>
            {getRatings()}
          </Users>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={close}>Close</Button>
      </Modal.Footer>
    </Modal>
  )
}