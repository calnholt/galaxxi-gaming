import React, { useState } from "react";
import styled from "styled-components";
import { useCardResults } from "./CardResultsContext";
import { useUser } from "../user/UserContext";
import ReactTooltip from "react-tooltip";
import { CardRatingModal } from "./CardRatingModal";
import { getNumOfRatings, getStars, getTeamRating } from "./CardHelper";

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  justify-content: center;
`;
const CardContainer = styled.div`
  margin: 1rem 0.5rem;
  position: relative;
`;
const Title = styled.div`
  text-align: center;
`;
const Image = styled.img`
  width: 100%;
  max-height: 25rem;
`;
const HoverImage = styled.div`
  transition: color ease-in-out 0.08s;
  position: absolute;
  top: 118px;
  right: 108px;
  font-size: 67px;
  color: #3e3e3e;
`;

const RatingContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0.5rem;
  transition: opacity ease-in-out 0.08s;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
`;

export const CardResultsList: React.FC = () => {
  const { cardResultsState } = useCardResults();

  return (
    <Container>
      {cardResultsState?.cardsToDisplay?.map(card => {
        return (
          <CardResult card={card} key={card._key} />
        );
      })}
    </Container>
  );
};

type CardResultProps = {
  card: Record<string, any>,
}

export const CardResult: React.FC<CardResultProps> = ({ card }) => {
  const { userState } = useUser();
  const [hovering, setHovering] = useState<boolean>(false);
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const getTooltipText = (): string => {
    const numOfRatings = getNumOfRatings(card);
    if (numOfRatings === 0) {
      return `No Ratings`;
    }
    return `${getNumOfRatings(card)} Ratings | ${getTeamRating(card)} Average`;
  };

  return (
    <CardContainer key={card._key}>
      <Image
        onMouseOver={() => setHovering(!hovering)}
        onMouseLeave={() => setHovering(!hovering)}
        src={card.image}
      />
      <>
        <ReactTooltip
          place="bottom"
          effect="solid"
        >
        </ReactTooltip>
        <RatingContainer onClick={() => setModalOpen(getNumOfRatings(card) > 0 || userState?.isTeam())} data-tip={getTooltipText()}>{getStars(card, getTeamRating(card))}</RatingContainer>
      </>
      <CardRatingModal card={card} isOpen={modalOpen} close={() => setModalOpen(false)} />
    </CardContainer>
  );
};
