import React, { useReducer, createContext } from "react";
import { useContext } from "react";
import { SearchFields } from "./CardFilters";

type CardResultsState = {
  searchResults: Record<string, any>[],
  filteredResults: Record<string, any>[],
  cardsToDisplay: Record<string, any>[],
  searchFields: SearchFields,
};

type CardResultsContext = {
  cardResultsState: CardResultsState,
  dispatchCardResults: React.Dispatch<any>;
};

const initialState: CardResultsState = {
  searchResults: [],
  filteredResults: [],
  cardsToDisplay: [],
  searchFields: {setName: ["Everfest"]},
};

const CardResultsContext = createContext<CardResultsContext>({
  cardResultsState: initialState,
  dispatchCardResults: null,
});

const CardResultAction = {
  SEARCH: "SEARCH",
  CLEAR: "CLEAR", // TODO: make clear all filters
  FILTERS_UPDATED: "FILTERS_UPDATED",
  FILTER_RESULTS: "FILTER_RESULTS",
  UPDATE: "UPDATE",
};

const reducer = (state: CardResultsState, action: any) => {
  switch(action.type) {
    case CardResultAction.SEARCH:
      return {
        ...state,
        searchResults: action.payload,
        cardsToDisplay: action.payload,
      };
    case CardResultAction.FILTERS_UPDATED:
      const fieldsCopy = {...state.searchFields, ...action.payload};
      Object.keys(action.payload).forEach(key => {
        if (!action.payload[key] || !action.payload[key].length) {
          delete fieldsCopy[key];
        }
      });
      return {
        ...state,
        searchFields: {...fieldsCopy},
      };
    case CardResultAction.FILTER_RESULTS:
      return {
        ...state,
        filteredResults: action.payload,
        cardsToDisplay: action.payload,
      };
    case CardResultAction.UPDATE:
      const updatedCardsToDisplay = {...state.cardsToDisplay};
      const updatedSearchResults = {...state.searchResults};
      const updateFilteredCards = {...state.filteredResults};
      let index = state.cardsToDisplay.findIndex(c => c._key === action.payload._key);
      if (index >= 0) {
        updatedCardsToDisplay[index] = action.payload;
      }
      index = state.searchResults.findIndex(c => c._key === action.payload._key);
      if (index >= 0) {
        updatedSearchResults[index] = action.payload;
      }
      index = state.filteredResults.findIndex(c => c._key === action.payload._key);
      if (index >= 0) {
        updateFilteredCards[index] = action.payload;
      }
      return {
        ...state,
        cardsToDisplay: updatedCardsToDisplay,
        filteredCards: updateFilteredCards,
        searchResults: updatedSearchResults,
      };
    default:
      return {...state};
  }
};

const CardResultsContextProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <CardResultsContext.Provider
      value={{
        cardResultsState: state,
        dispatchCardResults: dispatch,
      }}
    >
      {children}
    </CardResultsContext.Provider>
  );
};

const useCardResults = (): CardResultsContext => {
  const context = useContext<CardResultsContext>(CardResultsContext);

  if (context === undefined) {
    throw new Error("useCardResults must be used within a CardResultsContextProvider");
  }

  return context;
};

export { CardResultsContextProvider, useCardResults, CardResultAction};
