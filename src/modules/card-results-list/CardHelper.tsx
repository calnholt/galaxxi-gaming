import { faStar, faStarHalf } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { PURPLE } from "src/constants/Styles";
import { TEAM_MEMBERS } from "src/constants/Team";

export const getTeamRating = (card: Record<string, any>): number => {
  let ratings = 0;
  let ratingTotal = 0;
  TEAM_MEMBERS.forEach(t => {
    const rating = card[`${t}rating`];
    if (rating) {
      ratings++;
      ratingTotal += rating;
    }
  });
  if (ratingTotal > 0 && ratings > 0) {
    var rounded = Math.pow(10, 1);
    return Number((Math.round((ratingTotal / ratings) * rounded) / rounded).toFixed(1));
  }
  return -1;
};

export const getNumOfRatings = (card: Record<string, any>): number => {
  let ratings = 0;
  TEAM_MEMBERS.forEach(t => {
    const rating = card[`${t}rating`];
    if (rating) {
      ratings++;
    }
  });
  return ratings;
}

export const getStars = (card: Record<string, any>, rating: number) => {
  if (rating === -1) {
    return [...Array(5).keys()].map((x) => {
      return (
        <FontAwesomeIcon 
          size={"2x"} 
          key={x} 
          color={"#dddddd"} 
          icon={faStar} 
        />
      )
    })
  }
  const isHalf = String(rating).includes(".");
  return [...Array(Math.floor(rating)).keys()].map((x) => {
    return (
      <FontAwesomeIcon 
        size={"2x"} 
        key={x} 
        color={PURPLE} 
        icon={faStar} 
      />
    )
  }).concat(isHalf ? 
    <FontAwesomeIcon 
      size={"2x"} 
      color={PURPLE} 
      icon={faStarHalf} /> 
    : <></>
  );
};