import { useEasybase } from "easybase-react";
import React, { useEffect, useState } from "react";
import { Card } from "../common/Card";

export const Sponsors: React.FC = () => {
  const { db } = useEasybase();
  const [sponsors, setSponsors] = useState<Record<string, any>[] | number[]>([]);

  useEffect(() => {
    db('SPONSORS').return().orderBy({ by: "seq", sort: "asc" }).all().then(data=> {
      setSponsors(data);
    });
  }, []);

  return (
    <>
      {sponsors.map((s, i) => {
        return (
          <Card
            key={s.src}
            src={s.src}
            title={s.name}
            isRight={false}
            text={s.text}
          />
        )
      })}
    </>
  )
}