import { useEasybase } from "easybase-react";
import React, { useEffect, useState } from "react";
import { Card } from "../common/Card";

export const Team: React.FC = () => {
  const { db } = useEasybase();
  const [team, setTeam] = useState<Record<string, any>[] | number[]>([]);

  useEffect(() => {
    db('TEAM').return().orderBy({ by: "lastname", sort: "asc" }).all().then(data=> {
      setTeam(data);
    });
  }, []);

  return (
    <>
      {team.map((t, i) => {
        return (
          <Card
            key={t.src}
            src={t.src}
            title={`${t.firstname} ${t.lastname}`}
            isRight={i % 2 === 1}
            text={t.bio}
          />
        )
      })}
    </>
  )
}