import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { INTRO_VIDEO } from "src/constants/VideoConstants";
import styled from "styled-components";
import { EmbeddedVideo } from "../common/EmbeddedVideo";


const VideoContainer = styled.div`
  padding: 0px 200px 0px 200px;
`;

const HideButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 5px;
`;

const GalaxxiGamingIntroVideoStyles: React.CSSProperties = {
  position: "absolute",
  top: 0,
  left: 0,
  width: "100%",
  height: "100%",
};


export const IntroVideo = () => {
  const [isIntroHidden, setIsIntroHidden] = useState<boolean>(Boolean(localStorage.getItem("HIDE_INTRO")))

  const hideVideo = () => {
    localStorage.setItem("HIDE_INTRO", "true");
    setIsIntroHidden(true);
  };

  useEffect(() => {
  }, [isIntroHidden]);

  return (
    <VideoContainer>
      {true &&
        <>
          <EmbeddedVideo src={INTRO_VIDEO} style={GalaxxiGamingIntroVideoStyles}/>
          <HideButtonContainer>
            {/* <Button 
              variant="outline-primary"
              onClick={hideVideo}
            >
              Hide Video
            </Button> */}
          </HideButtonContainer>
        </>
      }
    </VideoContainer>
  )
}