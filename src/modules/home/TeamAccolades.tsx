import { useEasybase } from "easybase-react";
import React, { useEffect, useState } from "react";
import { PURPLE } from "src/constants/Styles";
import styled from "styled-components";
import { Card } from "../common/Card";

const Title = styled.div`
  font-size: 5rem;
  font-weight: 500;
  text-align: center;
  color: ${PURPLE};
`;

export const TeamAccolades: React.FC = () => {
  const { db } = useEasybase();
  const [accolades, setAccolades] = useState<Record<string, any>[] | number[]>([]);

  useEffect(() => {
    db('ACCOLADES').return().orderBy({ by: "accoldateDate", sort: "desc" }).all().then(data => {
      setAccolades(data);
    });
  }, [])

  return (
    <>
      <Title>Team Accolades</Title>
      {accolades.map((a, i) => {
        return (
          <Card 
            key={a._key} 
            src={a.src} 
            text={a.text} 
            isRight={i % 2 === 1} 
          />
        )
      })
      }
    </>
  )

}