import React from "react";
import styled from "styled-components";
import { IntroVideo } from "./IntroVideo";
import { TeamAccolades } from "./TeamAccolades";


const Section = styled.div`
  margin-top: 100px;
  margin-bottom: 100px;
`;
const HomeView: React.FC = () => {

  return (
    <> 
      <Section>
        <IntroVideo />
      </Section>
      <Section>
        <TeamAccolades />
      </Section>
    </>
  )
};

export default HomeView;