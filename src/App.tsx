import React from 'react';
import './App.css';
import { EasybaseProvider } from 'easybase-react';
import ebconfig from './ebconfig';
import styled from 'styled-components';
import { CardListView } from './modules/card-results-list/CardListView';
import { Header } from './modules/common/Header';
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Login from './modules/login/Login';
import HomeView from './modules/home/HomeView';
import { Footer } from './modules/common/Footer';
import { Team } from './modules/team/Team';
import { LOGIN, MEET_THE_TEAM, SPOILERS, SPONSORS } from './constants/RouteConstants';
import { Sponsors } from './modules/sponsors/Sponsors';
import { UserContextProvider } from './modules/user/UserContext';

const Container = styled.div`
  min-height: 83vh;
  max-width: 1520px;
  margin: 5rem 9rem 1rem 9rem;
  position: relative;
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
`;

const App = () => {
  return (
    <>
      <BrowserRouter basename={"/"}>
        <EasybaseProvider ebconfig={ebconfig}>
          <UserContextProvider>
            <Header />
            <Container>
              <Switch>
                <Route path={"/"} exact component={HomeView}></Route>
                <Route path={SPOILERS} exact component={CardListView}></Route>
                <Route path={LOGIN} exact component={Login}></Route>
                <Route path={MEET_THE_TEAM} exact component={Team}></Route>
                <Route path={SPONSORS} exact component={Sponsors}></Route>
              </Switch>
            </Container>
            <Footer />
          </UserContextProvider>
        </EasybaseProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
